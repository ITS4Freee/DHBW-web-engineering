import {Pipe} from "@angular/core";

@Pipe({
  name:"explode"

})
export class ExplodePipe {
  transform(value) {
    return value.split('-');
  }
}
