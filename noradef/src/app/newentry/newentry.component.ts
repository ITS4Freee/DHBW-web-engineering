import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-newentry',
  templateUrl: './newentry.component.html',
  styleUrls: ['./newentry.component.scss']
})
export class NewentryComponent implements OnInit {
  // declaration & Initalisation
  token;
  entry = {
    token: '',
    friendFullname: '',
    birthday: '',
    hobbies: [],
    nickname: '',
    mobPhone: '',
    starsign: '',
    chinStarsign: '',
    favoriteIncenseScent: '',
    ageByFirstSuperNatural: '',
    celestialBodyConstellationByBirth: {celestialBody: '', sign: ''},
    patronumAnimal: '',
    favoriteMovieSeries: '',
    favoriteSport: '',
    favoriteReadable: '',
    favoriteAnimal: '',
    favoriteFood: '',
    favoriteMusician: '',
    favoriteGame: '',
    mostUsedWebsite: ''
  };
  error = false;
  errormsg = '';
  hobbies;
  constructor(private data: DataService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token'); // get GET-Parameters from URls
    this.entry.token = this.token; // save this token in predefined entry object that is passed on later
  }
  submitEntry() {
    this.entry.hobbies = this.hobbies.split(','); // explode
    this.data.addNewEntry(this.entry).subscribe((res ) => { // add entry
      this.router.navigate(['/entry/' + res.id]); // if Entry is submited show entry
      },
      err => {
      // error handling
        this.error = true;
        this.errormsg = err.error;
       // console.log(err);
      });
   // console.log(this.entry);
  }
}

