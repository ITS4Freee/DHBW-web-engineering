import { Component } from '@angular/core';
import { LoginComponent} from './login/login.component';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  Title = 'Friendsbook';
}
