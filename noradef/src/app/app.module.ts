import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostComponent } from './post/post.component';
import { UsersComponent } from './users/users.component';
import { LoginComponent } from './login/login.component';
import { EntryComponent } from './entry/entry.component';
import { MyentryComponent } from './myentry/myentry.component';
import { EntrylistComponent } from './entrylist/entrylist.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from "@angular/forms";
import { NewentryComponent } from './newentry/newentry.component';
import { LinkComponent } from './link/link.component';
@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    UsersComponent,
    LoginComponent,
    EntryComponent,
    MyentryComponent,
    EntrylistComponent,
    NewentryComponent,
    LinkComponent
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
