import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {ActivatedRoute} from '@angular/router';
import {Entry} from '../Entry';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit {
  // Declare & Initalize
  error = false;
  errormsg = '';
  entry;
  id;
  constructor(private data: DataService, private route: ActivatedRoute ) {}
  ngOnInit() {
    // on load get parameter from link
    this.id = this.route.snapshot.paramMap.get('id');
    //get Entry with the given ID
   this.data.getEntryById(this.id).subscribe((res: any) => {
   this.entry = res; // define response to be the entry that is read && displayed
   }, err => {
     // error handling
     this.error = true; // diplay error
     this.errormsg = err.error; // display error msg
     //console.log(err.error);

   });
  }

}
