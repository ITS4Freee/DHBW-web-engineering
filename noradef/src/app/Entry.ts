import {CelestialBodyConstellationByBirth} from './CelestialBodyConstellationByBirth';

export class Entry {
  id: number;
  token: string;
  friendFullname: string;
  birthday: string;
  hobbies: string[];
  nickname: string;
  mobPhone: string;
  zodiac: string;
  chinZodiac: string;
  favoriteIncenseScent: string;
  ageByFirstSuperNatural: number;
  CelestialBodyConstellationByBirth: CelestialBodyConstellationByBirth;
  patronumAnimal: string;
  favoriteMovieSeries: string;
  favoriteSport: string;
  favoriteReadable: string;
  favoriteAnimal: string;
  favoriteFood: string;
  favoriteMusician: string;
  favoriteGame: string;
  mostUsedWebsite: string;
}
