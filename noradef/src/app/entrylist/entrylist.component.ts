import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {generate, Observable} from 'rxjs';
import {Entry} from '../Entry';
import {Token} from '../Token';
import {Router} from "@angular/router";

@Component({
  selector: 'app-entrylist',
  templateUrl: './entrylist.component.html',
  styleUrls: ['./entrylist.component.scss']
})
export class EntrylistComponent implements OnInit {
  // Declare & Initalize
  // List_of_Entriesid$: string[];
  // lengthid: number;
  // addEntryToken: string;
  // artifact  end
  linkgenerated = false;
  deleteEntryObject = {loginToken: '', id: ''};
  error = false;
  errormsg = '';
  List_of_Entries$: any[];
advancedsettings = false;
  token = localStorage.getItem('token');
  token2 =  this.token ;
  tokentosend: Object = { loginToken : this.token2};
  user = {username: '', password: ''};
  link;
  i = 0;
  ids: string[];
  id: string;
  constructor(private data: DataService, private router: Router ) { }
  ngOnInit() {
    // console.log(this.tokentosend);
    // get an array of all  Entries of the the Person that has this Token ( got from session / localstorage)
    this.data.getListOfEntries(this.tokentosend ).subscribe(
      response => { this.List_of_Entries$ = response;   // Positive response write Observable array to local array to be displayed
       // console.log('here');
       // console.log(this.List_of_Entries$);
        // this.fillListOfEntries();    ---Artifact of old code  might be needed again
        // console.log('here');
      }
    );
  }
  /*   More artifact Code
  fillListOfEntries() {
    this.lengthid = this.List_of_Entriesid$.length;
    console.log (this.i );
    console.log( this.lengthid);
    for(this.id in this.List_of_Entriesid$) {
      console.log('in Funtion' );
      console.log(this.List_of_Entries$[this.i]);
      this.data.getEntryById(this.id).subscribe(
        response => {
          for(this.id in response){
          }
          console.log(response); }, err => {
          this.error = true;
          this.errormsg = err;
          console.log(err);
        }
      );
      this.i++;
    }
  }*/
// generate the Link that is passed to the friends
generateLink() {
    // console.log('generating link');
    this.data.generateToken(this.tokentosend).subscribe(res  => { // call backend to generate token
      // console.log(res);
      // display the generated Link
      this.linkgenerated = true;
       return this.link = 'http://localhost:4200/newEntry/' + res.token; // create link with url + Token as get Parameter
    }, err => {
      // error handling display error
      this.error = true;
      this.errormsg = err;
  });
}
// delete Account
deleteAccount() {
    // console.log('account beeing deleted');
    // console.log(this.tokentosend);
    this.data.deleteUser(this.tokentosend).subscribe( // Delete a user that has a given token token from Session
      res => { console.log(res);
      }, err => {
        // Error Handling
        this.error = true;
        this.errormsg = err;
        // console.log(err);
      });
  this.router.navigate(['']); // kick user out of login
  }
  deleteEntry(entryID ) {
    // console.log('entry being deleted');
    // console.log(this.tokentosend);
    // fill Object that is send to Backend :
    this.deleteEntryObject.id = entryID;
    this.deleteEntryObject.loginToken = localStorage.getItem('token');
    this.data.deleteEntry(this.deleteEntryObject).subscribe( // Delete Entry with given ID and check if your authorised wizth token
      res => {
        console.log(res);
      },
        err => {
        // error handling
          console.log(err)
        this.error = true;
        this.errormsg = err;
       // console.log(err);
      });
    this.router.navigate(['/mybook ']);
    window.location.reload();
  }
}
