import {Component, NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsersComponent} from './users/users.component';
import {EntrylistComponent} from './entrylist/entrylist.component';
import {MyentryComponent} from './myentry/myentry.component';
import {LoginComponent} from './login/login.component';
import {EntryComponent} from './entry/entry.component';
import {AppComponent} from './app.component';
import {E} from '@angular/core/src/render3';
import {NewentryComponent} from './newentry/newentry.component';
import {PostComponent} from './post/post.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'newEntry/:token', component: NewentryComponent},
  {path: 'link' , component : PostComponent},
  {path: 'mybook', component: EntrylistComponent},
  {path: 'entry/:id', component: EntryComponent},
  {path: 'myentry', component: MyentryComponent},
  {path: 'test', component: UsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
