import { Component, OnInit, Input } from '@angular/core';
import {DataService} from '../data.service';
import {User} from '../User';
import {Observable, ObservableLike} from 'rxjs';
import { FormsModule } from '@angular/forms';
import {errorHandler} from '@angular/platform-browser/src/browser';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {tokenKey} from '@angular/core/src/view';
import {register} from 'ts-node';
import {Entry} from '../Entry';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  // Consolelogs are still in case we need to debug Last Minute
  title = 'Noradef'; // Display Title
  register = 0;     // Is Incremented when you want to register, to distinguish register & login
  @Input()
    // declare && Initialize
  user: User = {'username': '', 'password': ''}; // pre set object to be filled and sent via post
  token: string;
  // error & Success handling
  error = false;
  success = false;
  errormsg = 'Something went wrong, Sorry.¯\\_(ツ)_/¯ ';
  succmsg = 'You good.Please login¯\\_(ツ)_/¯ ';

  constructor(private data: DataService, private router: Router) {}
  ngOnInit() { }
  // send Login to authenticate reads from the Input fields
  sendlogin() {
    if (this.register > 0) { // distinguish if registration of login
      this.data.registerUser(this.user)
        .subscribe((res: any) => {
          // console.log(res.status);
          // console.log(res);
          // console.log(res);
          // show Success for Registration
            this.succmsg = 'Now, please Login.';
            this.success = true;
          }, err => {
          // error in Registration
          this.error = true;
          this.errormsg = 'Registration failed' + err;
        }
        );
      this.register = 0;// restet Registration to login nexttime
      // this.data.fetchUser().subscribe((res: Response) => {console.log(res.status + res.statusText + res.body); });
      // console.log('fetching user');
      //  this.data.register(this.user).subscribe((res: Response) => {
      //  console.log(res.statusText + 'HELLLO HERE');
      // });
      // this.data.fetchUser();
    } else { // now Login
      this.data.postCredentials(this.user) // take user data form form  send over service
        .subscribe((res: any) => {
            this.token = res.loginToken.token; // set return into token
            // console.log('res Statustest:' + res.statusText + 'res head:' + res.headers + 'res oks:' + res.ok);
            // console.log(res);
            // console.log(res.url);
            // console.log(res.type);
            if (res.loginToken.token != null) {
              // Session storage for further use of this data
              localStorage.setItem('token', this.token);
              localStorage.setItem('username', this.user.username);
              localStorage.setItem('password', this.user.password);
              // console.log(localStorage.getItem('token'));
              // console.log(res.body + 'body');

              this.router.navigate(['/mybook']); // forward to auth bereich
            }}
          ,
            err => {
          // error Handling
              this.error = true;
              this.errormsg = err.error;

          }
        );
      }
  }
}

