import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {User} from './User';
import {Observable, of} from 'rxjs';
import {errorHandler} from '@angular/platform-browser/src/browser';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule, RequestOptions} from '@angular/http';
import {Entry} from './Entry';
import { catchError, map, tap } from 'rxjs/operators';
import {HttpParamsOptions} from '@angular/common/http/src/params';
import {headersToString} from 'selenium-webdriver/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  // Declare & init
  ssd;
  httpOptions = {headers: new HttpHeaders({'Content-Type':  'application/JSON'})};
  url_login = 'http://localhost:80/login' ;
  url_entries = 'http://localhost:80/entry';
  url_Userhandling = 'http://localhost:80/person';
  url_basic = 'http://localhost:80/';
  constructor(private http: HttpClient) { }
  // log and  handle error function are not usefull anymore sice error handling is done on the go in the calling class
  // almost every call returns Observable <any> because that way they can be read from directls / can be forced to fit to classes
  getEntryById(id: string ): Observable<any> {
    console.log('getEntrybyid has been called with :' + id);
    return this.http.get<any>(this.url_entries + '/' + id);
  }
  getListOfEntries(token: Object) {
    return this.http.post<string[]>(this.url_Userhandling + '/get-entries', token).pipe(
      catchError(this.handleError('getListOfEntries', []))
    );
  }
  postCredentials(user: User): Observable<any> {
      return this.http.post<any>(this.url_login, user , this.httpOptions ).pipe(
      tap(
        data =>  console.log(data),
        error => console.log(error )
      ),
      catchError(this.handleError('login' , this.ssd))
    );

  }
  // Not implemented Yet
  updateEntry(entry: Entry ) {
    return this.http.put(this.url_entries, entry, this.httpOptions).pipe(
      catchError(this.handleError('updateEntry', entry))
    );
  }
  addNewEntry(entry): Observable<any> {
    return this.http.post<any>(this.url_entries, entry, this.httpOptions);
  }
  getSelfEntry(token: Object) {
    return this.http.post(this.url_Userhandling + '/get-self-entry', token);
  }
  generateToken(token: Object): Observable<any> {
    return this.http.post<any>(this.url_basic + 'generate-token', token);
  }
  // good Call
  setSelfEntry(entry) {
    return this.http.post(this.url_Userhandling + '/set-self-entry', entry);
  }
  // call that should egt a makeover
  registerUser(user: User): Observable<any> {
  return this.http.post<any>(this.url_Userhandling, user, this.httpOptions).pipe(
    tap( data => console.log(data),
    error => error ),
    catchError(this.handleError('registerUser'))
  );
  }
  // delete calls had to be post because Backend expects tokens in Body not Head
  deleteUser(token: any) {
    console.log(token);
  return this.http.post(this.url_Userhandling + '/delete', token);
  }
  deleteEntry(token: Object) {
    return this.http.post(this.url_entries + '/delete', token).pipe(
      catchError(this.handleError('deleteUser'))
    );
  }
/*
   postCredentials(user: User) {
    console.log(user);
      return this.http.post('http://localhost:80/login', user, this.httpOptions);

     // if login false display error

   }
   register(user: User) {
    return this.http.post(this.url_login, user, this.httpOptions).pipe(catchError(this.handleError()));
   }
   getEntrybyId(id: string) {
    return this.http.get<Entry>('https://jsonplaceholder.typicode.com/posts' + id).pipe(
      tap(_ => console.log('fetched Entry id=${id}')), catchError(this.handleError<Entry>('getEntrybyID id=${id}'))
    );
   }
  addEntry(entry: Entry) {
    return this.http.post(this.url_entry, entry) // .pipe(
      //  catchError(this.handleError<Entry>('HELLLLP'))
      // )
      ;
    // if login false display error

  }
  getEntries(username: string): Observable<Entry[]> {
    return this.http.get<Entry[]>('localhost:3200')
      .pipe(
      );

  }
  updateEntry(entry: Entry) {
    return this.http.put(this.url_entry + entry.id, entry).pipe();
  }
  addHero(user: User): Observable<User> {
    // @ts-ignore
    return this.http.post<User>(this.heroesUrl, user)
      .pipe(
        // catchError(this.handleError('addHero', user))
      );
  } */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }
  private log(message: string) {
    console.log(message);
  }
/*
  private handleError(addHero1: string, user: User[]) {
    console.log(addHero1 +"failed");
    console.log(user);
  }*/
}
