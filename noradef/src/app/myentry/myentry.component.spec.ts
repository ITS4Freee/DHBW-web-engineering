import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyentryComponent } from './myentry.component';

describe('MyentryComponent', () => {
  let component: MyentryComponent;
  let fixture: ComponentFixture<MyentryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyentryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyentryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
