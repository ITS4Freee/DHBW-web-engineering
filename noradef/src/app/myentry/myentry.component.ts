import { Component, Pipe, PipeTransform, OnInit } from '@angular/core';
import {Entry} from '../Entry';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../data.service';

import {CelestialBodyConstellationByBirth} from '../CelestialBodyConstellationByBirth';

@Pipe({
  name: 'explode'
})
export class ExplodePipe implements PipeTransform {
  transform(value) {
    return value.split(',');
  }
}

@Component({
  selector: 'app-myentry',
  templateUrl: './myentry.component.html',
  styleUrls: ['./myentry.component.scss']
})
export class MyentryComponent implements OnInit {
  // Declare and Initalize
  entry = {
    loginToken: '',
    newEntry: {
      friendFullname: '',
      birthday: '',
      hobbies: [],
      nickname: '',
      mobPhone: '',
      starsign: '',
      chinStarsign: '',
      favoriteIncenseScent: '',
      ageByFirstSuperNatural: '',
      celestialBodyConstellationByBirth: ['', ''],
      patronumAnimal: '',
      favoriteMovieSeries: '',
      favoriteSport: '',
      favoriteReadable: '',
      favoriteAnimal: '',
      favoriteFood: '',
      favoriteMusician: '',
      favoriteGame: '',
      mostUsedWebsite: ''
    }
  };
  hobbies; // hilfsvariable um hobbies aus zu bearbeiten
  // get Token from Session & save local
  token = localStorage.getItem('token');
  token2 =  this.token ;
  tokentosend: Object = { loginToken : this.token2};
  // Error handling Variables
  error = false;
  errormsg = '';
  constructor(private data: DataService, private router: Router, private route: ActivatedRoute) { }
  ngOnInit() {
    this.loadEntryonload(); // Get previous Selfentry to display prpevious value and to make it possible to update specific parts
  }
  loadEntryonload() {
    this.data.getSelfEntry(this.tokentosend) // get Selfentry by passing the token recieve ID
      .subscribe((res: any) => {
       this.loadEntrywithId(res.selfEntryID); // take the recieved ID and load the entry specific for that ID
      }, err => {
        // Error handling
        this.error = true;
        this.errormsg = err.error;
        // console.log(err.error);
      });
  }
  loadEntrywithId(resultPrevCall) {
    this.data.getEntryById(resultPrevCall).subscribe( // get entry by ID from the prev call
      res  => {
        // console.log(res);
        this.entry.newEntry = res;  // save entry in a local variable
        this.hobbies = this.entry.newEntry.hobbies.toString(); // implode string by "," and display
      }, err => {
        // Error handling
        this.error = true;
        this.errormsg = err.error;
        // console.log(err);
      }
    );

  }
  submitMyentry() {   // save the selfentry
    this.entry.loginToken = this.token2;
    this.entry.newEntry.hobbies = this.hobbies.split(','); // explode string to arra by ","
    // console.log(this.hobbies);
   // console.log(this.entry);
    this.data.setSelfEntry(this.entry).subscribe((res: Response) => {
      // console.log(res.status);
    this.router.navigate(['/mybook']); }, // send/save  selfentry if 200 than get thrown back to mybook
        err => {
      // Error handling
          this.error = true;
          this.errormsg = err.error;
          // console.log(err);
        });
    // console.log(this.entry);
}}

