import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
  getRegisterButton(){
    return element(by.buttonText('Register'));
  }
  getLoginButton(){
    return element(by.buttonText('Login'));
  }
  getUsernameInput(){
    return element(by.name('username'));
  }
  getPasswordInput(){
    return element(by.name('pw'));
  }
  getGenerateLink(){
    return element(by.name('generateLink'));
  }
  getGeneratedLink(){
    return element(by.name('generatedLink'));
  }
  getErrorMessage(){
    return element(by.className('bg-danger error'));
  }
  getSuccess(){
    return element(by.className('bg-success succ'));
  }
  proofSiteEntry(){
    return element(by.name('title'));
  }
  // Entry
  getYournameInput(){
    return element(by.name('yourname'));
  }
  getBirthdayInput(){
    return element(by.name('birthday'));
  }
  getHobbiesInput(){
    return element(by.name('hobbies'));
  }
  getNicknameInput(){
    return element(by.name('nickname'));
  }
  getMobileInput(){
    return element(by.name('mobPhone'));
  }
  getzodiacInput(){
    return element(by.name('zodiac'));
  }
  getChinesezodiacInput(){
    return element(by.name('chinStarsign'));
  }
  getIncenseInput(){
    return element(by.name('favoriteIncenseScent'));
  }
  getSupernaturalInput(){
    return element(by.name('ageByFirstSuperNatural'));
  }
  getSignInput(){
    return element(by.name('sign'));
  }
  getPlanetInput(){
    return element(by.name('planetList'));
  }
  getPatronumInput(){
    return element(by.name('patronomAnimal'));
  }
  getMovieInput(){
    return element(by.name('favoriteMovieSeries'));
  }
  getSportInput(){
    return element(by.name('favoriteSport'));
  }
  getBookInput(){
    return element(by.name('Readable'));
  }
  getGameInput(){
    return element(by.name('favoriteGame'));
  }
  getAnimalInput(){
    return element(by.name('favoriteAnimal'));
  }
  getFoodInput(){
    return element(by.name('favoriteFood'));
  }
  getMusicianInput(){
    return element(by.name('favoriteMusician'));
  }
  getWebsiteInput(){
    return element(by.name('mostUsedWebsite'));
  }
  getSubmitEntryButton(){
    return element(by.name('submitNewEntry'));
  }
  getFactualInformation(){
    return element(by.name('Factual Information'));
  }
  getDeleteDiv(){
    return element(by.name('divDeleteUser'));
  }
  getDeleteUser(){
    return element(by.name('DeleteUser'));
  }
  getDeleteEntry(){
    return element(by.name('deleteME'));
  }
  getName(){
    return element(by.name('Yourname'));
  }

}
