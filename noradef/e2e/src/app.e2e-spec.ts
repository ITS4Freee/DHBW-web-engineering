import { AppPage } from './app.po';
import {injectElementRef} from "@angular/core/src/render3";
import {inject} from "@angular/core";
import {getTestBed} from "@angular/core/testing";
import {browser, by} from "protractor";


describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('NORADEF');
  });
  it('should register user', () => {
    page.navigateTo();
    page.getUsernameInput().sendKeys('test');
    page.getPasswordInput().sendKeys('test');
    expect(page.getRegisterButton()).toBeTruthy('failedd');
  page.getRegisterButton().click();
  expect(page.getSuccess().getText()).toEqual('Now, please Login.' );

  });
  it('should Login user', () => {
    page.navigateTo();
    page.getUsernameInput().sendKeys('test');
    page.getPasswordInput().sendKeys('test');
    expect(page.getLoginButton()).toBeTruthy('No Login Button found');
    page.getLoginButton().click();
    expect(page.getParagraphText()).toEqual('Entries in My Personal Friendsbook!');

  });
  it('should make new link user', () => {
    page.getGenerateLink().click();
    page.getGeneratedLink().click();

    expect(page.proofSiteEntry().getText()).toEqual('Add a new Entry to Your Friends Friendsbook');

  });
  it('should  send new Entry', () => {
    page.getYournameInput().sendKeys('Ramon');
    page.getBirthdayInput().sendKeys('02.11.1998');
    page.getHobbiesInput().sendKeys('Ramon');
    page.getNicknameInput().sendKeys('02.11.1998');
    page.getMobileInput().sendKeys('Ramon');
    page.getzodiacInput().element(by.cssContainingText('option', 'Scorpion')).click();
    page.getChinesezodiacInput().sendKeys('Ox');
    page.getIncenseInput().sendKeys('vanilla');
    page.getSupernaturalInput().sendKeys('4');
    page.getSignInput().sendKeys('Aries');
    page.getPlanetInput().sendKeys('sun');
    page.getPatronumInput().sendKeys('deer');
    page.getBookInput().sendKeys('Harry potter');
    page.getMovieInput().sendKeys('The Godfarther');
    page.getAnimalInput().sendKeys('Corgy');
    page.getSportInput().sendKeys('tennis');
    page.getGameInput().sendKeys('Valliant Hearts');
    page.getFoodInput().sendKeys('Burger');
    page.getMusicianInput().sendKeys('Queen');
    page.getWebsiteInput().sendKeys('www.google.com');
    page.getSubmitEntryButton().click();
    browser.pause();
    expect(page.getParagraphText()).toEqual('You are reading the Entry of Ramon');

  });
  it('should Login user', () => {
    page.navigateTo();
    page.getUsernameInput().sendKeys('test');
    page.getPasswordInput().sendKeys('test');
    expect(page.getLoginButton()).toBeTruthy('No Login Button found');
    page.getLoginButton().click();
    expect(page.getParagraphText()).toEqual('Entries in My Personal Friendsbook!');

  });
  it('should Login user', () => {
    page.navigateTo();
    page.getUsernameInput().sendKeys('test');
    page.getPasswordInput().sendKeys('test');
    expect(page.getLoginButton()).toBeTruthy('No Login Button found');
    page.getLoginButton().click();
    expect(page.getParagraphText()).toEqual('Entries in My Personal Friendsbook!');

  });

});
