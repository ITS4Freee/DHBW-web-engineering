const express = require("express");
const app = express();
const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const adapter = new FileSync('db.json')
const db = low(adapter);
const cookieParser = require('cookie-parser');
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json({extended : true}));

db.defaults({persons: [], entries: []})
  .write();

function checkString(object){
    var args = arguments;
    for(var i=0; i<args.length; i++){
        object = args[i];
        if(object == undefined || object.length == 0){
            return false;
        }
    }
    return true;
}

//register a new person
app.post("/person", function(req, res){
    var person = req.body;
    //if input is valid, proceed. Otherwise pop an error in users face
    if(checkString(person["username"], person["password"])){
        var username_already_taken = false;
        for(var i = 0; i < db.get("persons").value().length; i++){
            if(person["username"] == db.get("persons").value()[i]["username"]){
                username_already_taken = true;
            }
        }
        if(username_already_taken){
            res.status(409);
            res.send("username taken");
        }
        else{
            var newPerson = {};
            newPerson["username"] = person["username"];
            newPerson["password"] = person["password"];
            newPerson["selfEntryID"] = 0;
            newPerson["entries"] = [];
            newPerson["tokens"] = [];
            newPerson["loginToken"] = "";
            db.get("persons").push(newPerson).write();
            res.send("everything okay");
        }
    }
    else{
        res.status(405);
        res.send("invalid input");
    }
});


//update a person
app.put("/person", function(req, res){
    var loginToken = req.body["loginToken"];
    var newPerson = req.body["person"];
    if(checkString(loginToken, newPerson, newPerson["username"], newPerson["password"], newPerson["selfEntryID"])){
        var person;
        for(var i = 0; i < db.get("persons").value().length; i++){
            if(db.get("persons").value()[i]["loginToken"]["token"] == loginToken){
                person = db.get("persons").value()[i];
            }
        }
        if(person == undefined){
            res.status(401);
            res.send("not authorized")
        }
        else{
            var entries = person["entries"];
            var tokens = person["tokens"];
            db.get("persons").pull(person).write();
            var writePerson = {};
            writePerson["username"] = newPerson["username"];
            writePerson["password"] = newPerson["password"];
            writePerson["selfEntryID"] = newPerson["selfEntryID"];
            writePerson["entries"] = entries;
            writePerson["tokens"] = tokens;
            writePerson["loginToken"] = "";
            db.get("persons").push(writePerson).write();
            res.send("everything okay");
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

//remove a person // we needed to change this from app.delete to app .post because angular cant send infomation in the body of an delete request
app.post("/person/delete", function(req, res){
    var loginToken = req.body["loginToken"];
    if(checkString(loginToken)){
        var person;
        for(var i = 0; i < db.get("persons").value().length; i++){
            if(db.get("persons").value()[i]["loginToken"]["token"] == loginToken){
                person = db.get("persons").value()[i];
            }
        }
        if(person == undefined){
            res.status(401);
            res.send("not authorized")
        }
        else{
            db.get("persons").pull(person).write();
            res.send("everything okay");
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

app.post("/login", function(req, res){
    var credentials = req.body;

    if(checkString(credentials["username"], credentials["password"])){
        //if everything works fine
        if(db.get("persons").filter(credentials).value().length == 1){
            //create login token
            var alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var index;
            var token = "";
            for(var i = 0; i < 16; i++){
                index = Math.floor(Math.random() * 62);
                token = token + alphabet[index];
            }
            //6h validity
            var token_valid_until = (new Date()).getTime() + 1000*60*60*6;
            var token_object = {"loginToken": {"token": token, "validUntil": token_valid_until}};
            db.get("persons").find(credentials).assign(token_object).write();
            res.send(token_object);
        }
        //problem
        else{
            res.status(401);
            res.send("not authorized")
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

//get entries
app.post("/person/get-entries", function(req, res){
    var loginToken = req.body["loginToken"];
    if(checkString(loginToken)){
        var person;
        for(var i = 0; i < db.get("persons").value().length; i++){
            if(db.get("persons").value()[i]["loginToken"]["token"] == loginToken){
                person = db.get("persons").value()[i];
            }
        }
        if(person == undefined){
            res.status(401);
            res.send("not authorized")
        }
        else{
            var entries = [];
            for (var i = 0; i < person["entries"].length; i += 1) {
              entries.push(db.get("entries").find({"id": person["entries"][i]}).value());
            }
            res.send(entries)
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

//get entries
app.post("/person/get-self-entry", function(req, res){
    var loginToken = req.body["loginToken"];
    if(checkString(loginToken)){
        var person;
        for(var i = 0; i < db.get("persons").value().length; i++){
            if(db.get("persons").value()[i]["loginToken"]["token"] == loginToken){
                person = db.get("persons").value()[i];
            }
        }
        if(person == undefined){
            res.status(401);
            res.send("not authorized")
        }
        else{
            selfEntryObject = {"selfEntryID": person["selfEntryID"]};
            if(person["selfEntryID"] == 0){
                res.status(404);
                res.send("no self entry yet");
            }
            else{
                res.send(selfEntryObject);
            }
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

app.post("/generate-token", function(req, res){
    var loginToken = req.body["loginToken"];
    if(checkString(loginToken)){
        var person;
        for(var i = 0; i < db.get("persons").value().length; i++){
            if(db.get("persons").value()[i]["loginToken"]["token"] == loginToken){
                person = db.get("persons").value()[i];
            }
        }
        if(person == undefined){
            res.status(401);
            res.send("not authorized")
        }
        else{
            var alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var index;
            var token = "";
            for(var i = 0; i < 16; i++){
                index = Math.floor(Math.random() * 62);
                token = token + alphabet[index];
            }
            var tokens = person["tokens"];
            tokens.push(token);
            db.get("persons").find(person).assign({"tokens":tokens}).write();
            res.send({"token":token});
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

app.post("/entry/delete", function (req, res) {
  var loginToken = req.body["loginToken"];
  var id = req.body["id"];
  if (checkString(loginToken)) {
    var person;
    for (var i = 0; i < db.get("persons").value().length; i += 1) {
      if (db.get("persons").value()[i]["loginToken"]["token"] === loginToken) {
        person = db.get("persons").value()[i];
      }
    }
    if (person === undefined) {
      res.status(401);
      res.send("not authorized");
    } else if (db.get("entries").filter({ "id": id }).value().length === 1) {
      var entry = db.get("entries").find({ "id": id }).value();
      db.get("entries").pull(entry).write();
      for (var k = 0; k < db.get("persons").value().length; k += 1) {
        var entries = db.get("persons").value()[k]["entries"];
        for (var j = 0; j < entries.length; j += 1) {
          if (entries[j] === id) {
            // pull id out of entries and selfEntryID if it is in selfEntryID
            db.get("persons").nth(k).get("entries").pull(id).write();
            if (db.get("persons").value()[k]["selfEntryID"] === id) {
              db.get("persons").nth(k).assign({ "selfEntryID": 0 }).write();
            }
            res.send("everything okay");
            break;
          }
        }
      }
    } else {
      console.log(db.get("entries").filter({ "id": id }).value());
      res.status(404);
      res.send("entry not found");
    }
  } else {
    res.status(405);
    res.send("invalid input");
  }
});

app.post("/entry", function (req, res) {
  var bd = req.body;

    //is this entry a valid one?
    var valid_req = true;
    var keys = "friendFullname birthday hobbies nickname mobPhone starsign "
        + "chinStarsign favoriteIncenseScent ageByFirstSuperNatural "
        + "celestialBodyConstellationByBirth patronumAnimal "
        + "favoriteMovieSeries favoriteSport favoriteReadable "
        + "favoriteGame favoriteAnimal favoriteFood favoriteMusician mostUsedWebsite";

    for(var k = 0; k < keys.split(" ").length; k++){
        key = keys.split(" ")[k];
        if(!checkString(bd[key])){
            valid_req = false;
            console.log("key " + key + " is missing");
            break;
        }
    }
    if(valid_req){
        var valid_token = false;
        //check which person issued this token
        personLoop:
        for(var i = 0; i < db.get("persons").value().length; i++){
            for(var j = 0; j < db.get("persons").value()[i]["tokens"].length; j++){
                if(db.get("persons").value()[i]["tokens"][j] == bd["token"]){

                    //delete token so it can't be used twice
                    db.get("persons").nth(i).get("tokens").pull(bd["token"]).write();
                    var entry = {};

                    //find lowest free entryID to ues
                    var entries = db.get("entries").value();
                    var lowestID;
                    //iterate through every number from lowest to highest potential ID
                    for(var k = 1; true; k++){
                        var exists = false;
                        //has any entry already id == i?
                        for(var l = 0; l < entries.length; l++){
                            if(entries[l]["id"] == k){
                                exists = true;
                                break;
                            }
                        }
                        //if not then we use this one
                        if(!exists){
                            lowestID = k;
                            break;
                        }
                    }

                    //transfer every valid key-value pair to new cleansed object
                    entry["id"] = lowestID;

                    for(var k = 0; k < keys.split(" ").length; k++){
                        key = keys.split(" ")[k];
                        entry[key] = bd[key];
                    }

                    var personEntries = db.get("persons").value()[i]["entries"];
                    personEntries.push(lowestID);
                    db.get("persons").nth(i).assign({"entries":personEntries}).write();

                    db.get("entries").push(entry).write();
                    res.send(entry);
                    valid_token = true;
                    break personLoop;
                }
            }
        }
        if(!valid_token){
            res.status(401);
            res.send("invalid token");
        }
    }
    else{
        res.status(405);
        res.send("invalid input")
    }
});

app.post("/person/set-self-entry", function (req, res) {
  var bd = req.body;

  // is this entry a valid one?
  var validReq = true;
  var keys = "friendFullname birthday hobbies nickname mobPhone starsign "
        + "chinStarsign favoriteIncenseScent ageByFirstSuperNatural "
        + "celestialBodyConstellationByBirth patronumAnimal "
        + "favoriteMovieSeries favoriteSport favoriteReadable "
        + "favoriteGame favoriteAnimal favoriteFood favoriteMusician mostUsedWebsite";

  for (var keyIndex = 0; keyIndex < keys.split(" ").length; keyIndex += 1) {
    var key = keys.split(" ")[keyIndex];
    if (!checkString(bd["newEntry"][key])) {
      validReq = false;
      break;
    }
  }
  if (validReq) {
    var validAuthToken = false;
    // check which person issued this token
    for (var i = 0; i < db.get("persons").value().length; i += 1) {
      if (db.get("persons").value()[i]["loginToken"]["token"] === bd["loginToken"]) {
        // bd["newEntry"];
        var newEntry = {};
        var oldID = db.get("persons").value()[i]["selfEntryID"];

        // is this an update or a new creation?
        if (oldID !== 0) {
          newEntry["id"] = oldID;
          var entry = db.get("entries").find({ "id": oldID }).value();
          db.get("entries").pull(entry).write();
        } else {
          // find lowest free entryID to use
          var entries = db.get("entries").value();
          var lowestID;
          // iterate through every number from lowest to highest potential ID
          for (var k = 1; true; k += 1) {
            var exists = false;
            // has any entry already id == i?
            for (var l = 0; l < entries.length; l += 1) {
              if (entries[l]["id"] === k) {
                exists = true;
                break;
              }
            }
            // if not then we use this one
            if (!exists) {
              lowestID = k;
              break;
            }
          }

          newEntry["id"] = lowestID;
        }


        // transfer every valid key-value pair to new cleansed object
        for (var m = 0; m < keys.split(" ").length; m += 1) {
          newEntry[keys.split(" ")[m]] = bd["newEntry"][keys.split(" ")[m]];
        }

        // when new creation, update ids in person object?
        if (oldID === 0) {
          // update ids in person object
          var personEntries = db.get("persons").value()[i]["entries"];
          personEntries.push(newEntry["id"]);
          db.get("persons").nth(i).assign({ "entries": personEntries }).write();
          db.get("persons").nth(i).assign({ "selfEntryID": newEntry["id"] }).write();
        }

        // push new entry
        db.get("entries").push(newEntry).write();
        res.send(newEntry);

        validAuthToken = true;
        break;
      }
    }
    if (!validAuthToken) {
      res.status(401);
      res.send("not authorized");
    }
  } else {
    res.status(405);
    res.send("invalid input");
  }
});


app.get("/entry/:id", function (req, res) {
  var id = parseInt(req.params.id, 10);
  if (db.get("entries").filter({ "id": id }).value().length === 1) {
    var entry = db.get("entries").find({ "id": id }).value();
    res.send(entry);
  } else {
    res.status(404);
    res.send("entry not found");
  }
});

//decide for one of the two
app.use(express.static('frontend'));
app.get('*', function(req, res) {
    res.sendfile('./frontend/');
});

app.listen(80, function () {
    console.log("Server listening on port 80!");
});
